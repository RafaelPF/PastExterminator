//
//  Drone.swift
//  Ar21
//
//  Created by Rafael  Passos Farias on 08/05/18.
//  Copyright © 2018 Rafael  Passos Farias. All rights reserved.
//

import ARKit

class Drone: SCNNode {
    func loadModel() {
        guard let virtualObjectScene = SCNScene(named: "Drone.scn") else { return }
        let wrapperNode = SCNNode()
        for child in virtualObjectScene.rootNode.childNodes {
            wrapperNode.addChildNode(child)
        }
        addChildNode(wrapperNode)
    }
}
