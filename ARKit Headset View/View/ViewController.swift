import SceneKit
import ARKit
import SpriteKit
import AVFoundation


enum BodyType:Int {
    case text = 1
    case text2 = 2
    case model = 4
    case borracha = 8
    case SketchUp = 16
    case shipMesh = 32
    case municao = 64
}

class ViewController: UIViewController, ARSCNViewDelegate, SCNPhysicsContactDelegate{
    let kStartingPosition = SCNVector3(0, 0, -0.6)
    let kAnimationDurationMoving: TimeInterval = 0.2
    let kMovingLengthPerLoop: CGFloat = 0.05
    let kRotationRadianPerLoop: CGFloat = 0.2
    var currenteMunicaoNode: SCNNode?
    var cam: SKCameraNode?
    var player: SKSpriteNode?
    var cameraNode: SCNNode!
    var shelfNode: SCNNode!
    var baseCanNode: SCNNode!
    let lista = ["Borracha","Óculos", "Enxergar", "Lupa", "Apagar","Proteção","Aumentar","ver"]
    var audioPlayer: AVAudioPlayer!
    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet weak var sceneViewLeft: ARSCNView!
    @IBOutlet weak var sceneViewRight: ARSCNView!
    @IBOutlet weak var imageViewLeft: UIImageView!
    @IBOutlet weak var imageViewRight: UIImageView!
    let eyeCamera : SCNCamera = SCNCamera()
    let interpupilaryDistance = 0.066 // This is the value for the distance between two pupils (in metres). The Interpupilary Distance (IPD).
    let viewBackgroundColor : UIColor = UIColor.black // normal is UIColor.white
    var scene:SCNScene!
    var plane:SCNNode!
    /*
     SET eyeFOV and cameraImageScale. UNCOMMENT any of the below lines to change FOV:
     */
    //    let eyeFOV = 38.5; var cameraImageScale = 1.739; // (FOV: 38.5 ± 2.0) Brute-force estimate based on iPhone7+
    let eyeFOV = 60; var cameraImageScale = 3.478; // Calculation based on iPhone7+ // <- Works ok for cheap mobile headsets. Rough guestimate.
    //    let eyeFOV = 90; var cameraImageScale = 6; // (Scale: 6 ± 1.0) Very Rough Guestimate.
    //    let eyeFOV = 120; var cameraImageScale = 8.756; // Rough Guestimate.
    @objc var municao:SCNNode?
    var repeatTimer:Timer!
    var repeatTimer2:Timer!
    override func viewDidLoad() {
        super.viewDidLoad()
        sceneView.delegate = self
        sceneView.showsStatistics = true
        // Create a new scene
        scene = SCNScene(named: "art.scnassets/ship.scn")!
        plane = scene.rootNode.childNode(withName: "plane", recursively: true)
        
        // create a crosshair
        plane?.position = SCNVector3(x: 0, y: 0, z: -1.5)
        
        
        
        repeatTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(ViewController.dispenseNewMunicao), userInfo: nil, repeats: true)
        showToast()
        showToast2()
        // Set the scene to the view
        sceneView.scene = scene
        // App Setup
        UIApplication.shared.isIdleTimerDisabled = true
        // Scene setup
        sceneView.isHidden = true
        self.view.backgroundColor = viewBackgroundColor
        // Set up Left-Eye SceneView
        sceneViewLeft.scene = scene
        //sceneViewLeft.scene = scene2
        //sceneViewLeft.showsStatistics = sceneView.showsStatistics
        sceneViewLeft.isPlaying = true
        
        // Set up Right-Eye SceneView
        sceneViewRight.scene = scene
        // sceneViewRight.scene = scene2
        //sceneViewRight.showsStatistics = sceneView.showsStatistics
        sceneViewRight.isPlaying = true
        sceneView.pointOfView?.addChildNode(plane!)
        //node.scale
        // Update Camera Image Scale - according to iOS 11.3 (ARKit 1.5)
        if #available(iOS 11.3, *) {
            print("iOS 11.3 or later")
            cameraImageScale = cameraImageScale * 1080.0 / 720.0
        } else {
            print("earlier than iOS 11.3")
        }
        
        // Create CAMERA
        eyeCamera.zNear = 0.001
        /*
         Note:
         - camera.projectionTransform was not used as it currently prevents the simplistic setting of .fieldOfView . The lack of metal, or lower-level calculations, is likely what is causing mild latency with the camera.
         - .fieldOfView may refer to .yFov or a diagonal-fov.
         - in a STEREOSCOPIC layout on iPhone7+, the fieldOfView of one eye by default, is closer to 38.5°, than the listed default of 60°
         */
        eyeCamera.fieldOfView = CGFloat(eyeFOV)
        sceneView.showsStatistics = true
        //sceneView.debugOptions = ARSCNDebugOptions.showWorldOrigin
        //setupScene()
        
        configureLighting()
        
        // Setup ImageViews - for rendering Camera Image
        self.imageViewLeft.clipsToBounds = true
        self.imageViewLeft.contentMode = UIViewContentMode.center
        self.imageViewRight.clipsToBounds = true
        self.imageViewRight.contentMode = UIViewContentMode.center
    }
    func physicsWorld(_ world: SCNPhysicsWorld, didBegin contact: SCNPhysicsContact) {
        print ("Tocou!")
    }
    
    @objc func dispenseNewMunicao() {
        //sceneView.pointOfView?.addChildNode(municao!)
        municao = scene.rootNode.childNode(withName: "municao", recursively: true)
        municao?.physicsBody?.restitution = 1
//        municao?.physicsBody?.categoryBitMask = BodyType.municao.rawValue
//        municao?.physicsBody?.collisionBitMask = BodyType.borracha.rawValue
//        municao?.physicsBody?.contactTestBitMask = BodyType.borracha.rawValue
        municao?.position = SCNVector3(x: 0, y: 0, z: -0.9)
//        let municaoScene = SCNScene(named: "art.scnassets/municao.dae")!
//        let municaoNode = municaoScene.rootNode.childNode(withName: "municao", recursively: true)!
        //        municaoNode.name = "municao"
        //let municaoPhysicsBody = SCNPhysicsBody()
        //municaoPhysicsBody.mass = 3
        // municaoPhysicsBody.friction = 2
        //municao?.physicsBody?.contactTestBitMask = 1
        //municao?.physicsBody = municaoPhysicsBody
        // municao?.position = SCNVector3(x: 0, y: 0, z: -0.8)
        //municao?.physicsBody?.applyForce(SCNVector3(x: (sceneView.pointOfView?.position.x)!, y: (sceneView.pointOfView?.position.y)!, z: (sceneView.pointOfView?.position.z)! * -10), asImpulse: true)
        let action = SCNAction.move(to: SCNVector3(x:  plane!.position.x, y:  plane!.position.y, z: -10), duration: 1)
        //currenteMunicaoNode = municao
        sceneView.pointOfView?.addChildNode(municao!)
        municao?.runAction(action)
        playSound()
        
    }
    func configureLighting() {
        sceneView.autoenablesDefaultLighting = true
        sceneView.automaticallyUpdatesLighting = true
    }//essa func add luz
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        setupConfiguration()
        // Run the view's session
        sceneView.session.run(configuration)
    }
    func setupConfiguration() {
        let configuration = ARWorldTrackingConfiguration()
        sceneView.session.run(configuration)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    // MARK: - ARSCNViewDelegate
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        DispatchQueue.main.async {
            self.updateFrame()
        }
    }
//    //add uma trajetoria
//    func getRay(for point: CGPoint, in view: SCNSceneRenderer) -> SCNVector3 {
//        let farPoint  = view.unprojectPoint(SCNVector3(Float(point.x), Float(point.y), 1))
//        let nearPoint = view.unprojectPoint(SCNVector3(Float(point.x), Float(point.y), 0))
//
//        let ray = SCNVector3Make(farPoint.x - nearPoint.x, farPoint.y - nearPoint.y, farPoint.z - nearPoint.z)
//
//        // Normalize the ray
//        let length = sqrt(ray.x*ray.x + ray.y*ray.y + ray.z*ray.z)
//
//        return SCNVector3Make(ray.x/length, ray.y/length, ray.z/length)
//    }
//    //add tambem uma trajetoria
//    func sceneSpacePosition(inFrontOf node: SCNNode, atDistance distance: Float) -> SCNVector3 {
//        let localPosition = SCNVector3(x: 0, y: 0, z: -distance)
//        let scenePosition = node.convertPosition(localPosition, to: nil)
//        return scenePosition
//    }
    func updateFrame() {
        // CREATE POINT OF VIEWS
        let pointOfView : SCNNode = SCNNode()
        pointOfView.transform = (sceneView.pointOfView?.transform)!
        pointOfView.scale = (sceneView.pointOfView?.scale)!
        // Create POV from Camera
        pointOfView.camera = eyeCamera
        
        // Set PointOfView for SceneView-LeftEye
        sceneViewLeft.pointOfView = pointOfView
        
        // Clone pointOfView for Right-Eye SceneView
        let pointOfView2 : SCNNode = (sceneViewLeft.pointOfView?.clone())!
        // Determine Adjusted Position for Right Eye
        let orientation : SCNQuaternion = pointOfView.orientation
        let orientationQuaternion : GLKQuaternion = GLKQuaternionMake(orientation.x, orientation.y, orientation.z, orientation.w)
        let eyePos : GLKVector3 = GLKVector3Make(1.0, 0.0, 0.0)
        let rotatedEyePos : GLKVector3 = GLKQuaternionRotateVector3(orientationQuaternion, eyePos)
        let rotatedEyePosSCNV : SCNVector3 = SCNVector3Make(rotatedEyePos.x, rotatedEyePos.y, rotatedEyePos.z)
        let mag : Float = Float(interpupilaryDistance)
        pointOfView2.position.x += rotatedEyePosSCNV.x * mag
        pointOfView2.position.y += rotatedEyePosSCNV.y * mag
        pointOfView2.position.z += rotatedEyePosSCNV.z * mag
        
        // Set PointOfView for SceneView-RightEye
        sceneViewRight.pointOfView = pointOfView2
        
        ////////////////////////////////////////////
        // RENDER CAMERA IMAGE
        /*
         Note:
         - as camera.contentsTransform doesn't appear to affect the camera-image at the current time, we are re-rendering the image.
         - for performance, this should ideally be ported to metal
         */
        // Clear Original Camera-Image
        sceneViewLeft.scene.background.contents = UIColor.clear // This sets a transparent scene bg for all sceneViews - as they're all rendering the same scene.
        
        // Read Camera-Image
        let pixelBuffer : CVPixelBuffer? = sceneView.session.currentFrame?.capturedImage
        if pixelBuffer == nil { return }
        let ciimage = CIImage(cvPixelBuffer: pixelBuffer!)
        // Convert ciimage to cgimage, so uiimage can affect its orientation
        let context = CIContext(options: nil)
        let cgimage = context.createCGImage(ciimage, from: ciimage.extent)
        
        // Determine Camera-Image Scale
        var scale_custom : CGFloat = 1.0
        // let cameraImageSize : CGSize = CGSize(width: ciimage.extent.width, height: ciimage.extent.height) // 1280 x 720 on iPhone 7+
        // let eyeViewSize : CGSize = CGSize(width: self.view.bounds.width / 2, height: self.view.bounds.height) // (736/2) x 414 on iPhone 7+
        // let scale_aspectFill : CGFloat = cameraImageSize.height / eyeViewSize.height // 1.739 // fov = ~38.5 (guestimate on iPhone7+)
        // let scale_aspectFit : CGFloat = cameraImageSize.width / eyeViewSize.width // 3.478 // fov = ~60
        // scale_custom = 8.756 // (8.756) ~ appears close to 120° FOV - (guestimate on iPhone7+)
        // scale_custom = 6 // (6±1) ~ appears close-ish to 90° FOV - (guestimate on iPhone7+)
        scale_custom = CGFloat(cameraImageScale)
        
        // Determine Camera-Image Orientation
        let imageOrientation : UIImageOrientation = (UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeLeft) ? UIImageOrientation.down : UIImageOrientation.up
        
        // Display Camera-Image
        let uiimage = UIImage(cgImage: cgimage!, scale: scale_custom, orientation: imageOrientation)
        self.imageViewLeft.image = uiimage
        self.imageViewRight.image = uiimage
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else{return}
        let result = sceneView.hitTest(touch.location(in: sceneView), types: [ARHitTestResult.ResultType.featurePoint])
        guard let hitResult = result.last else {return}
        let hitTransform = SCNMatrix4(hitResult.worldTransform)
        let hitVector = SCNVector3Make(hitTransform.m41, hitTransform.m42, hitTransform.m43)
        createBall(position: hitVector)
    }
    func createBall(position : SCNVector3){
        
        
        // para adiacionar um textinho
        let random = Int(arc4random_uniform(UInt32(self.lista.count)))
        let ballShape = SCNText(string:self.lista[random] , extrusionDepth: 0.5)
        let ballNode = SCNNode(geometry: ballShape)
        let font = UIFont(name:"Futura", size: 1)
        ballShape.font = font
        ballShape.alignmentMode = kCAAlignmentCenter
        ballShape.firstMaterial?.diffuse.contents = UIColor.red
        ballShape.firstMaterial?.specular.contents = UIColor.white
        ballShape.firstMaterial?.isDoubleSided = true
        ballShape.chamferRadius = 0.01
        
        let (minBound, maxBound) = ballShape.boundingBox
        ballNode.pivot = SCNMatrix4MakeTranslation( (maxBound.x - minBound.x)/2, minBound.y, 0.02/2)
        ballNode.scale = SCNVector3Make(0.1, 0.1, 0.1)
        
        ballNode.position = position
        sceneView.scene.rootNode.addChildNode(ballNode)
    }
    func showToast() {
        repeatTimer2 = Timer.scheduledTimer(withTimeInterval: 45, repeats: false) { (timer) in
            // do stuff 30 seconds later
            let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/4 - 75, y: self.view.frame.size.height-280, width: 150, height: 35))
            toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            toastLabel.textColor = UIColor.white
            toastLabel.numberOfLines = 0
            toastLabel.adjustsFontSizeToFitWidth = true
            toastLabel.textAlignment = .center;
            toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
            toastLabel.text = "Você não pode apagar o passado."
            toastLabel.alpha = 1.0
            toastLabel.layer.cornerRadius = 10;
            toastLabel.clipsToBounds  =  true
            self.view.addSubview(toastLabel)
            UIView.animate(withDuration: 50000, delay: 3, options: .curveEaseOut, animations: {
                toastLabel.alpha = 0.0
            }, completion: {(isCompleted) in
                toastLabel.removeFromSuperview()
            })
        }
    }
    func showToast2() {
        repeatTimer2 = Timer.scheduledTimer(withTimeInterval: 20, repeats: false) { (timer) in
            // do stuff 30 seconds later
            let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/4 - 75, y: self.view.frame.size.height-130, width: 150, height: 35))
            toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            toastLabel.textColor = UIColor.white
            toastLabel.numberOfLines = 0
            toastLabel.adjustsFontSizeToFitWidth = true
            toastLabel.textAlignment = .center;
            toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
            toastLabel.text = "Mas pode aprender com ele. :)"
            toastLabel.alpha = 1.0
            toastLabel.layer.cornerRadius = 10;
            toastLabel.clipsToBounds  =  true
            self.view.addSubview(toastLabel)
            UIView.animate(withDuration: 50000, delay: 3, options: .curveEaseOut, animations: {
                toastLabel.alpha = 0.0
            }, completion: {(isCompleted) in
                toastLabel.removeFromSuperview()
            })
        }
    }
}



