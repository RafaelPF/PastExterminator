//
//  Sound.swift
//  ARKit Headset View
//
//  Created by Rafael  Passos Farias on 20/05/18.
//  Copyright © 2018 CompanyName. All rights reserved.
//

import Foundation
import AVFoundation

var player: AVAudioPlayer?

func playSound() {
    let url = Bundle.main.url(forResource: "somTiro", withExtension: "mp3")
    
    do {
        player = try AVAudioPlayer(contentsOf: url!)
        guard let player = player else { return }
        
        player.prepareToPlay()
        player.play()
    } catch let error as NSError {
        print(error.description)
    }
}


