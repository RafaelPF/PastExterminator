////
////  Alert.swift
////  ARKit Headset View
////
////  Created by Rafael  Passos Farias on 20/05/18.
////  Copyright © 2018 CompanyName. All rights reserved.
////
//
//import Foundation
//import UIKit
//
//func showToast(message : String) {
//    
//    let toastLabel = UILabel(frame: CGRect(x: view.frame.size.width/2 - 75, y: view.frame.size.height-100, width: 150, height: 35))
//    toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
//    toastLabel.textColor = UIColor.white
//    toastLabel.textAlignment = .center;
//    toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
//    toastLabel.text = message
//    toastLabel.alpha = 1.0
//    toastLabel.layer.cornerRadius = 10;
//    toastLabel.clipsToBounds  =  true
//    selfview.addSubview(toastLabel)
//    UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
//        toastLabel.alpha = 0.0
//    }, completion: {(isCompleted) in
//        toastLabel.removeFromSuperview()
//    })
//}
